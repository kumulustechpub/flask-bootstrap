# Base image
FROM python:3.9-slim-buster

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    OPENAI_API_KEY=${OPENAI_API_KEY}

# Set the working directory
WORKDIR /app

# Install dependencies
COPY requirements.txt /app/
RUN pip install -U pip setuptools
RUN pip install -r requirements.txt && mkdir -p /app/files/

# Copy the Flask app
COPY . /app/

# Expose the port
EXPOSE 80

# Run the Gunicorn server
CMD ["gunicorn", "--bind", "0.0.0.0:80", "--worker-class=gevent", "--worker-tmp-dir", "/dev/shm", "app:app"]

