# A simple Flask + Bootstrap App for ChatGPT

This app was built with support from OpenAI's ChatGPT (GPT4) model.

## Prerequisites

This app assumes 1password is installed and configured for use from the command line
This app also assumes that the user has an OpenAI API Key stored in their private 1password vault under the op://private/OpenAIapi/credentials path

## Goal

Provide an interface to ask for a response, and then copy that response to use elsewhere.
Provide an example of ChatGPT helping to build an app

## Prompts

can you create a python3 openai flask app that accepts a ChatGPT request and outputs the response with a bootstrap css ui

can we update the index.html template to render the output file as a propertly formatted file as well?

how can I update the index.html template to render the <br> in the jquery responses as breaks rather than as literal <br>

can you add a "one click copy" button to the index page so that I can easily copy the resulting text to use in a local file?

Can you create a Dockerfile and requirements.txt file to support running the application on port80 with the gunicorn runtime?

can you create a run.sh script that reads the OpenAIAPI key from the op 1password client and launches the container with that key

## Build and Run

```
docker build . -t myapp
./run.sh
```

