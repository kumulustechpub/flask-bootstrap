from flask import Flask, render_template, request, jsonify
import asyncio
import openai
import os
import re

openai.api_key = os.getenv("OPENAI_API_KEY")

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        message = request.form['message']
        response = chat_with_gpt(message)
        return jsonify(response=response)
    return render_template('index.html')


def chat_with_gpt(message):
    prompt = f"{message}"
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=prompt,
        max_tokens=150,
        n=1,
        stop=None,
        temperature=0.5,
    )
    text = response.choices[0].text.strip()
    formatted_text = re.sub(r'\n', '<br>', text)

    # Save response to a file
    with open("output.txt", "a") as output_file:
        output_file.write(f"User: {message}\n")
        output_file.write(f"ChatGPT: {text}\n")
        output_file.write("\n")

    return formatted_text

if __name__ == '__main__':
    app.run(debug=True)

