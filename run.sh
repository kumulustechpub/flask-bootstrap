#!/bin/bash

# Get the OpenAI API key from 1Password
OPENAI_API_KEY=$(op read op://private/OpenAIapi/credential)
if [ -z $OPENAI_API_KEY ]; then echo "No API key"; exit 1; fi
# Run the Docker image, mapping a local directory into the container
docker run -p 8080:80 -v ${PWD}/:/app/files/ -e OPENAI_API_KEY="$OPENAI_API_KEY" myapp

